'use strict';
// Parse the json data from data.js
const data = JSON.parse(json);

// Declare Constants
const timerContainer = take('#timer');
const optionsContainer = take('#options');
const maxBackgroundImages = 11;

// Declare Variables
let countdown,
countdownTimer;
// Settings Object
let settingsData = {
  introTextTitle: 'Herzlich Willkommen',
  introTextSubTitle: '',
  eventDescription: 'Gottesdienst',
  versFontSize: '9',
  animDuration: '30',
  ctwn: false,
  targetTime: '10:00',
  videoBgFile: 'bg-1.mp4'
}

// Declare Methods
function take(id, i) {
  if(!i) {
    if(document.querySelectorAll(id).length < 2) {
      return document.querySelector(id);
    } else {
      return document.querySelectorAll(id);
    }
  }

  return document.querySelectorAll(id)[i];
}

function rndBG() {
  let n = Math.round((Math.random() * maxBackgroundImages));
  if(n > maxBackgroundImages) return 1;
  return n;
}

function generateHTML(containerID, content) {
  take(containerID).innerHTML = content;
}

function getAnimElement(x) {
  take(x).style.animationDuration = `${settingsData.animDuration}s`;
}

function renderHTML() {
  getLocalStorage();
  
  // Generate HTML Contents
  // Monatsvers
  let content = `
  <div class="vers">
  <p>${monatsVersText}</p>
  </div>
  <div class="monatsvers">
  <p>${monatsvers} | Monatsvers ${monat}</p>
  </div>
  `;
  
  // Intro Description Line
  let datum = `
  <p>${day}. ${monat} ${year} | ${settingsData.eventDescription}</p>
  `;

  // Apply Video Bg File
  take('.background-video').src = `./data/${settingsData.videoBgFile}`;
    
  // Apply Font Size
  take('#intro-title').style.fontSize = fontSizeCalc(settingsData.introTextTitle.length, 1.2) + 'em';
  take('#intro-subtitle').style.fontSize = fontSizeCalc(settingsData.introTextSubTitle.length, 2) + 'em';
  take('#vers-container').style.fontSize = fontSizeCalc(monatsVersText.length, 1) + 'em';
      
  // Set animation Duration
  let animDurNr = Number(settingsData.animDuration)/2;
  getAnimElement('#background');
  getAnimElement('#slide-intro');
  getAnimElement('#slide-vers');
  take('#slide-vers').style.animationDelay = `${animDurNr.toString()}s`;
    
  // Output the HTML Content
  generateHTML('#intro-title', `<h1>${settingsData.introTextTitle}</h1>`);
  generateHTML('#intro-subtitle', `<h2>${settingsData.introTextSubTitle}</h2>`);
  generateHTML('#datum', datum);
  generateHTML('#vers-container', content);

  // Activate Countdown
  if(settingsData.ctwn) {
    take('#opt-countdown').checked = true;
  }
}

// Check Local Storage and load Data
function setLocalStorage() {
  if(!localStorage) {    
    take('#options').style.display = 'none';
    return;
    
  } else {
    
    if(!localStorage.getItem('settingsData')) {
      localStorage.setItem('settingsData', JSON.stringify(settingsData));      
    }
  }
}

function getLocalStorage() {
  // Get Items from Local Storage
  settingsData = JSON.parse(localStorage.getItem('settingsData'));
  
  // Set Values for Form Inputs
  take('#opt-title').value = settingsData.introTextTitle;
  take('#opt-subtitle').value = settingsData.introTextSubTitle;
  take('#opt-event').value = settingsData.eventDescription;
  take('#opt-font-size').value = settingsData.versFontSize;
  take('#opt-anim-duration').value = settingsData.animDuration;
  take('#opt-countdown-time').value = settingsData.targetTime;
  
}

function setData() {

  // Get Form Values
  settingsData.introTextTitle = take('#opt-title').value;
  settingsData.introTextSubTitle = take('#opt-subtitle').value;
  settingsData.eventDescription = take('#opt-event').value;
  settingsData.versFontSize = take('#opt-font-size').value;
  settingsData.animDuration = take('#opt-anim-duration').value;
  settingsData.targetTime = take('#opt-countdown-time').value;
  settingsData.ctwn = take('#opt-countdown').checked === true ? true : false;
  settingsData.videoBgFile = take('#opt-video-file').value;

  // Set Local Storage
  localStorage.setItem('settingsData', JSON.stringify(settingsData));

  renderHTML();
}

function setSwitchData() {
  if(this.id === 'intro') {
    settingsData.introTextTitle = 'Herzlich Willkommen';
    settingsData.introTextSubTitle = '';
    settingsData.eventDescription = 'Gottesdienst';

    // Set Local Storage
    localStorage.setItem('settingsData', JSON.stringify(settingsData));
    
    renderHTML();
    
  } 
  if(this.id === 'outro') {
    settingsData.introTextTitle = 'Auf Wiedersehen';
    settingsData.introTextSubTitle = 'Wir wünschen einen gesegneten Sonntag!';
    settingsData.eventDescription = 'Gottesdienst';

    // Set Local Storage
    localStorage.setItem('settingsData', JSON.stringify(settingsData));

    renderHTML();    
  }
  
}

function fontSizeCalc(length, divider) {
  // Set the font Size
  let mlt = Number(settingsData.versFontSize) / divider;

  if(length < 80) {
    let x = (mlt/10) * 3;
    return x.toString();
  }
  if(length < 140) {
    let x = (mlt/10) * 2.3;
    return x.toString();
  }
  if(length > 141) {
    let x = (mlt/10) * 1.3;
    return x.toString();
  }
}

function doubleDigit(val) {
  if(val<10) {
    return '0' + val.toString();
  } else {
    return val.toString();
  }
}

function generateTimer() {
  
  let intDate = new Date;
  let ctdwnMinute;
  let ctdwnSecond;

  let targetHour = Number(settingsData.targetTime.split(':')[0]);
  let targetMinute = Number(settingsData.targetTime.split(':')[1]);

  let actHour = intDate.getHours();
  let actMinute = intDate.getMinutes();
  let actSecond = intDate.getSeconds();

  let hourDif = targetHour - actHour;

  if(hourDif < 0) {
    targetMinute = (24 - actHour) * 60;
  }

  if(hourDif > 0) {
    targetMinute += (hourDif * 60);
  }

  ctdwnMinute = (targetMinute - 1) - actMinute;
  ctdwnSecond = 59 - actSecond;
  
  // Make Timer Visible
  if(settingsData.ctwn) {
    if(take('#opt-countdown-show').checked) {
      timerContainer.classList.add('active')
    } else {
      timerContainer.classList.remove('active')
    };

    if(ctdwnMinute < 5 && !timerContainer.classList.contains('active')) {
      timerContainer.classList.add('active');
    }
    
    // Make Time Big
    if(ctdwnMinute <= 1) {
      timerContainer.classList.add('big');
    } else {
      timerContainer.classList.remove('big');
    }

    // Blackout on Full Hour
    if(ctdwnMinute < 1 && ctdwnSecond == 0) {
      take('#wrapper').classList.add('black');
    }

    if(ctdwnMinute < 0) {
      timerContainer.classList.remove('active');
      window.clearInterval(countdownTimer);
    }
  } else {
    timerContainer.classList = '';
    window.clearInterval(countdownTimer);
  }

  take('#timer').innerHTML = `${doubleDigit(ctdwnMinute)} : ${doubleDigit(ctdwnSecond)}`;
}

function toggleFullScreen() {
  if (!take('#wrapper').mozFullScreenElement && take('#wrapper').mozRequestFullScreen) {
    take('#wrapper').mozRequestFullScreen();
  } else if (!take('#wrapper').webkitFullscreenElement && take('#wrapper').webkitRequestFullscreen) {
    take('#wrapper').webkitRequestFullscreen();
  } else if (!take('#wrapper').fullscreenElement && take('#wrapper').requestFullscreen) {
      take('#wrapper').requestFullscreen();
    } else {
    if (take('#wrapper').mozFullScreenElement) {
      take('#wrapper').mozCancelFullScreen()(); 
    }
    if (take('#wrapper').webkitFullscreenElement) {
      take('#wrapper').webkitExitFullscreen()(); 
    }
    if (take('#wrapper').fullscreenElement) {
      take('#wrapper').exitFullscreen()(); 
    }
  }
}

function tabHandler() {
  if(this.classList.contains('active')) {
    console.log(this.id + 'Is active')
    return;
  } else {
    let id = this.id.split('-')[2];
    if(id==='text') {
      take('#opt-tab-text').classList.add('active');
      take('#opt-tab-advanced').classList.remove('active');
      take('#opt-tablink-advanced').classList.remove('active');
    }
    if(id==='advanced') {
      take('#opt-tab-advanced').classList.add('active');
      take('#opt-tab-text').classList.remove('active');
      take('#opt-tablink-text').classList.remove('active');
    }
    this.classList.add('active');
  }
}

function keyEvents(e) {
  if (e.keyCode == 66) {  // B Taste
    take('#wrapper').classList.toggle('black');
  }
  if (e.keyCode == 32 || e.keyCode == 70) {  // Leertaste oder F
    toggleFullScreen();
  }
}

function removeOptionsPanel() {
  document.addEventListener('keydown', keyEvents);
  optionsContainer.classList.remove('visible');
  take('#options-button').classList.remove('active');
}


// End of Methods

// Add Event Handlers
// Set the Click Handlers for Options Menu
take('#open-options').addEventListener('click', function() {
  // Remove Key Inputs for Shortcuts to not interfere with Text Type
  if(optionsContainer.classList.contains('visible')) {
    document.addEventListener('keydown', keyEvents);
  } else {
    document.removeEventListener('keydown',keyEvents);
  }
  optionsContainer.classList.toggle('visible');
  take('#options-button').classList.toggle('active');
})

  // Set actions for Options Save Click
take('#opt-save').addEventListener('click', function() {
  document.addEventListener('keydown', keyEvents);
  setData();
  optionsContainer.classList.remove('visible');
  take('#options-button').classList.remove('active');
  if(take('#opt-countdown').checked) {
    countdownTimer = window.setInterval(generateTimer, 1000);
  }
  if(!take('#opt-countdown').checked) {
    take('#opt-countdown-show').checked = false;
  }
})

// Set actions for Options Cancel Click
take('#opt-cancel').addEventListener('click', removeOptionsPanel);

  // Set Click action for Word Templates
take('#intro').addEventListener('click', setSwitchData);
take('#outro').addEventListener('click', setSwitchData);

  // Set tabs click Handler
take('#opt-tablink-text').addEventListener('click', tabHandler);
take('#opt-tablink-advanced').addEventListener('click', tabHandler);

  // Set Click Handler for Black-Out Button
take('#open-blackscreen').addEventListener('click', function() {
  take('#wrapper').classList.toggle('black');
})

  // Set Click Handler for Fullscreen Button
take('#open-fullscreen').addEventListener('click', toggleFullScreen);

  // Set Key Events
document.addEventListener("keydown", keyEvents, false);

// Get the Date
let date = new Date();
let day = date.getDate();
let month = date.getMonth();
let year = date.getFullYear();

// Get the specific Month Data
let monat = data[month].name;
let monatsvers = data[month].Vers;
let monatsVersText = data[month].Text;

//Initialize Page
setLocalStorage();

//Set Background
let setBG = rndBG();
take('#bg').style.backgroundImage = `url('./data/bg-${setBG}.jpg')`;
take('#background').style.background = `url('./data/bg-${setBG}.jpg')`;
take('.background-video').src = `./data/${settingsData.videoBgFile}`;
take('#opt-video-bg').addEventListener('change', function(e) {
  if(this.checked) {
    take('.background-video').classList.add('visible');
    take('#opt-video-solo').style.opacity = 1;
    take('#background').style.background = 'none';
    take('.background-container').style.opacity = 0.7;
  } else {
    take('.background-video').classList.remove('visible');
    take('#opt-video-solo').style.opacity = 0;
    take('#background').style.background = `url('./data/bg-${setBG}.jpg')`;
    take('.background-container').style.opacity = 1;
  }
})
take('#opt-video-solo').addEventListener('change', function(e) {
  if(this.checked) {
    take('#slide-intro').classList.add('mute');
    take('#slide-vers').classList.add('mute');
    take('.background-container').classList.add('mute');
  } else {
    take('#slide-intro').classList.remove('mute');
    take('#slide-vers').classList.remove('mute');
    take('.background-container').classList.remove('mute');
  }
})

// Render Page and set Timer
renderHTML();
if(settingsData.ctwn) {
  countdownTimer =  window.setInterval(generateTimer, 1000);
};




const json = `
[
  {
    "name": "Januar",
    "Vers": "5. Mose 5, 14",
    "Text": "Der siebte Tag ist ein Ruhetag, dem Herrn, deinem Gott, geweiht.<br>An ihm darfst du keine Arbeit tun: du und dein Sohn und deine Tochter<br>und dein Sklave und deine Sklavin<br>und dein Rind und dein Esel<br>und dein ganzes Vieh und dein Fremder in deinen Toren."
  },
  {
    "name": "Februar",
    "Vers": "5. Mose 30, 14",
    "Text": "Es ist das Wort ganz nahe bei dir,<br>in deinem Munde und in deinem Herzen, dass du es tust."
  },
  {
    "name": "März",
    "Vers": "Johannes 19, 30",
    "Text": "Jesus Christus spricht:<br>Es ist vollbracht!"
  },
  {
    "name": "April",
    "Vers": "Johannes 20, 21",
    "Text": "Jesus Christus spricht: Friede sei mit euch!<br>Wie mich der Vater gesandt hat, so sende ich euch."
  },
  {
    "name": "Mai",
    "Vers": "Hebräer 11, 1",
    "Text": "Es ist aber der Glaube eine feste Zuversicht dessen,<br>was man hofft, und ein Nichtzweifeln an dem, was man nicht sieht."
  },
  {
    "name": "Juni",
    "Vers": "Hebräer 13, 2",
    "Text": "Vergesst die Gastfreundschaft nicht;<br>denn durch sie haben einige, ohne es zu ahnen, Engel beherbergt."
  },
  {
    "name": "Juli",
    "Vers": "Hosea 10, 12",
    "Text": "Säet Gerechtigkeit und erntet nach dem Maße der Liebe!<br>Pflüget ein Neues, solange es Zeit ist, den HERRN zu suchen,<br>bis er kommt und Gerechtigkeit über euch regnen lässt!"
  },
  {
    "name": "August",
    "Vers": "1. Johannes 4, 16",
    "Text": "Gott ist Liebe, und wer in der Liebe bleibt,<br>bleibt in Gott und Gott bleibt in ihm."
  },
  {
    "name": "September",
    "Vers": "Prediger 3, 11",
    "Text": "Gott hat alles schön gemacht zu seiner Zeit,<br>auch hat er die Ewigkeit in ihr Herz gelegt;<br>nur dass der Mensch nicht ergründen kann das Werk,<br>das Gott tut, weder Anfang noch Ende."
  },
  {
    "name": "Oktober",
    "Vers": "Psalm 38, 10",
    "Text": "Herr, all mein Sehnen liegt offen vor dir,<br>mein Seufzen war dir nicht verborgen."
  },
  {
    "name": "November",
    "Vers": "Offenbarung 21, 2",
    "Text": "Und ich sah die heilige Stadt, das neue Jerusalem,<br>von Gott aus dem Himmel herabkommen,<br>bereitet wie eine geschmückte Braut für ihren Mann."
  },
  {
    "name": "Dezember",
    "Vers": "Matthäus 2, 10",
    "Text": "Da sie den Stern sahen,<br>wurden sie hocherfreut."
  }
]`;